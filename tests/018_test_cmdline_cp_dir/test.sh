#!/bin/bash

function get_home() {
  if $(which alien.py &>/dev/null); then
    echo "$(alien.py pwd)"
  elif $(which aliensh &>/dev/null); then
    local username="$(alien_whoami | sed 's/ //')"
    echo "/alice/cern.ch/user/${username:0:1}/$username/"
  fi
}

ALIEN_HOME="$(get_home)"
WORKSPACE="${ALIEN_HOME}workspace" # home already includes slash at the end

function setup() {
  alien_mkdir "$WORKSPACE"
  mkdir local_src local_dst
  echo "hello" > local_src/fileA
  echo "world" > local_src/fileB
  alien_cp file:local_src/fileA "alien:$WORKSPACE/fileA@disk:1"
  alien_cp file:local_src/fileB "alien:$WORKSPACE/fileB@disk:1"
}

function teardown() {
  rm -r local_src local_dst

  if $(which aliensh &>/dev/null); then
    alien_rmdir "$WORKSPACE"
  elif $(which alien.py &>/dev/null); then
    alien_rm -r "$WORKSPACE"
  fi
}

function die() {
  teardown
  echo "JAliEn-CI test failed"
  exit 1
}

setup

alien_cp "alien:$WORKSPACE" file:local_dst/
test -e local_dst/workspace/fileA || die
test -e local_dst/workspace/fileB || die
echo "Test passed successfully!"

teardown
