#!/bin/bash
# NOTE: nhardi, 12. 10. 2018.
# This test is indirectly testing the precedence of loading the ROOT plugins.
# When TAlien has higher precedence, then opening/copying files using TJAlien,
# from local machine to the GRID fails.
# Until the legacy TAlien plugin is completely removed from ROOT, we must be extra
# careful about this.
root -b -x -q -l test_XML_files.C
