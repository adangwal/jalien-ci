int pwd_no_newline() {
  TGrid::Connect("alien");
  bool interactive = false;
  int stream = 3; // ignored by jalien-root
  TGridResult *r = gGrid->Command("pwd", interactive, stream);
  const char *path = r->GetKey(0, "pwd");
  int len = strlen(path);
  if(path[len-1] == '\n') {
    std::cout << "The pwd command return path with a newline character!\n";
    return 1;
  } else {
    return 0;
  }
  return 0;
}
