#!/bin/bash
############
replica_only
############

WORKSPACE="$(alien.py pwd)/workspace"
PAYLOAD="${WORKSPACE}/payload.sh"
JDL="${WORKSPACE}/sample.jdl"

function cleanup() {
    echo "Running cleanup"
    alien.py rm -r $WORKSPACE
}

function die() {
    # cleanup
    echo ${1:-"something went wrong"}
    exit 1
}

function get_last_status() {
    alien.py -json ps | jq -r .results[-1].status
}

function check_status() {
    expected="$1"
    real="$(get_last_status)"

    [ x"$expected" = x"$real" ] || return 1
}

alien_cp -f "file://payload.sh" "alien://$PAYLOAD"
alien_cp -f "file://sample.jdl" "alien://$JDL"

alien.py submit "$JDL" "$PAYLOAD"
check_status "INSERTING" || die "Job not in INSERTING state"

[ -f "$vol/optimiser.sh" ] || die "Optimiser script not present"
bash $vol/optimiser.sh -o
check_status "WAITING" || die "Job not in WAITING state"

for i in $(seq 120);
do
  check_status "WAITING" || break
  sleep 1
done

check_status "WAITING" && die "job still in WIAITING state, didn't run yet"
echo "Test succeeded"
exit 0
