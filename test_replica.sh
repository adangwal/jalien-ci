#!/bin/bash
set -e
set -x
export PATH=/cvmfs/alice.cern.ch/bin:$PATH
export REPLICA=1

JALIEN_CI_REPO=$PWD
ls

mkdir -p workspace
pushd workspace # START

START="$PWD"
SHARED_VOLUME="$PWD/shared"
mkdir -p $SHARED_VOLUME

# Get the code
set -x
echo "jalien setup repo: ${JALIEN_SETUP_REPO}"
git clone ${JALIEN_SETUP_REPO:-https://gitlab.cern.ch/jalien/jalien-setup.git} jalien-setup -b ${JALIEN_SETUP_BRANCH:-master}
git clone --depth 1   ${JALIEN_REPO:-https://gitlab.cern.ch/jalien/jalien.git} jalien       -b ${JALIEN_BRANCH:-master}
set +x

# Build jalien
pushd jalien
    ./compile.sh cs
    JAR_PATH=$PWD/alien-cs.jar
popd

# Start service
pushd jalien-setup
    make all
    set -x
    ./bin/jared --jar $JAR_PATH --volume $SHARED_VOLUME
    cd $SHARED_VOLUME
    cat .env
    cat env_setup.sh
    cat docker-compose.yml
    set +x
    docker-compose config
    docker-compose up -d
popd

# Run the tests
which alienv
# LATEST_JALIEN_ROOT=$(alienv q | grep -i jalien-root | sort -g | tail -n 1)

# TODO: set the environment in this shell
alienv setenv JAliEn-ROOT/0.5.5-25 -c ". $SHARED_VOLUME/env_setup.sh; cd $JALIEN_CI_REPO; while ! alien.py pwd; do sleep 2; done; ./integration_tests.sh --keep-going"

MYSQLCALL="mysql --verbose --user=root --host=JCentral-dev --password=pass --port=3307 -D mysql -e"
$MYSQLCALL 'insert into alice_users.INDEXTABLE values (4,2,0,"/alice/data/");'
$MYSQLCALL 'insert into alice_data.INDEXTABLE values (4,2,0,"/alice/data/");'
$MYSQLCALL "insert into alice_data.L0L values (46, 'admin', 0,'2011-10-06 17:07:26', NULL, NULL,NULL, 'alice/', 0, NULL, 0, 42, 'admin', 'd', NULL,NULL, 755);"
$MYSQLCALL "insert into alice_data.L0L values (47, 'admin', 0,'2011-10-06 17:07:26', NULL, NULL,NULL, 'alice/data/', 0, NULL, 0, 46, 'admin', 'd', NULL,NULL, 755);"
alienv setenv JAliEn-ROOT/0.5.5-25 -c ". $SHARED_VOLUME/env_setup.sh;alien.py cp ./tests/002_test_TFile_Copy/OCDBFoldervsRange.xml alien:///alice/data/"
alienv setenv JAliEn-ROOT/0.5.5-25 -c ". $SHARED_VOLUME/env_setup.sh;alien.py mv alien:///alice/data/OCDBFoldervsRange.xml alien:///alice/data/OCBDFoldervsRunRange.xml"
alienv setenv JAliEn-ROOT/0.5.5-25 -c ". $SHARED_VOLUME/env_setup.sh;echo 'toXml /alice/data/OCBDFoldervsRange.xml -x /alice/data/2018/LHC18b/000285396/muon_calo_pass1/18000285396019.100/AliESDs.root' | alien.py"

# Stop the service
pushd $SHARED_VOLUME
  docker-compose down -t 2
popd

popd # END
rm -rf workspace || true
